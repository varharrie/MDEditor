# MDEditor
-----
javascript实现简易内嵌markdown编辑器，可供学习参考。

![预览](./preview.png)

### 依赖：
- [jQuery](https://github.com/jquery/jquery)
- [marked](https://github.com/chjj/marked)
- [highlight](https://github.com/isagalaev/highlight.js)

### 使用：
#### 用法
```javascript
$('[selector]').initEditor([options]);
```
#### 选项
传入对象，可选以下属性：
- wrapperClass:'editor'
- toolbarClass:'editor-toolbar'
- toolBtnClass:'item'
- textareaId:''
- textareaClass:'editor-textarea'
- textareaHeight:'300px'
- previewClass:'editor-preview'
- indent:'    '
- tools:['bold','italic','header','ul','ol','code','quote','link','img','file','indent','preview','fullscreen']

#### 示例
```html
<link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="../styles/mdeditor-default.css">
<link rel="stylesheet" href="../lib/highlight/styles/monokai.css">
```
```html
<div id="editor"></div>
```
```javascript
<script src='../lib/jquery/jquery.min.js'></script>
<script src='../lib/marked/marked.min.js'></script>
<script src='../lib/highlight/highlight.min.js'></script>
<script>hljs.initHighlightingOnLoad();</script>
<script src='../mdeditor.js'></script>
<script>
	$('#editor').initEditor();
</script>
```

#### 结束语
最近在写个人博客，要用到markdown方面的技术，苦于现有的一些markdown编辑器都不太符合要求，要么是不支持内嵌，要么就是界面不搭。所以自己写了一个，主要参考了[suem](https://github.com/suemi994)的[suMarkdown](https://github.com/suemi994/suMarkdown)。